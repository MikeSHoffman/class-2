/*
 * Validate the email
 * You are given an email as string myEmail
 * make sure it is in correct email format.
 * Should be in this format, no whitespace:
 * 1 or more characters
 * @ sign
 * 1 or more characters
 * .
 * 1 or more characters
 */
var myEmail = 'foo@bar.baz'; // good email
var badEmail = 'badmail@gmail'; // bad email

const emailRegex = /^\w+@\w+\.\w+$/;

console.log(emailRegex.test(myEmail));
console.log(emailRegex.test(badEmail));
