// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price
const logReceipt = function(taxRate){
  let taxAmount = 0;
  let subtotal = 0;
  let total = 0;
  const args = Array.from(arguments);

  args.forEach(function(item) {
    if (!item.descr){
      tax = item;
    }
    else 
    {
      console.log(`${item.descr} - $${item.price}`); 
      subtotal += item.price;
    }
  });
  console.log(`Subtotal - $${subtotal}`);

  taxAmount = taxRate * subtotal;
  console.log(`Tax - $${taxAmount.toFixed(2)}`);

  total = subtotal + taxAmount;
  console.log(`Total - $${total.toFixed(2)}`);
}

// Check
logReceipt(0.1,
  {descr: 'Burrito', price: 5.99},
  {descr: 'Chips & Salsa', price: 2.99},
  {descr: 'Sprite', price: 1.99}
);
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Subtotal - $10.97
// Tax - $1.10
// Total - $12.07


// Change logReceipt so that the firstparameter is a taxRate which takes a number (i.e. 0.1 for 10% tax). 
// Any additional arguments will be objects with two properties, descr and price (same as above).
// Before logging the total, log a Subtotal and Tax.
