// Create a constructor function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`
const SpaceShip = function(name, topSpeed){

  // Make name and topSpeed private
  this.name = name;
  this.topSpeed = topSpeed;

  // Keep both name and topSpeed private, but 
  // add a method that changes the topSpeed
  this.changeTopSpeed = function(newTopSpeed){
    this.topSpeed = newTopSpeed;
  }

  this.accelerate = function(){
    console.log(`${this.name} moving to ${this.topSpeed}`);    
  }  
}

// Call the constructor with a couple ships, change the topSpeed
// using the method, and call accelerate.
const ship1 = new SpaceShip('Enterprise', 5);
console.log(`${ship1.name} accelerating...`);
ship1.accelerate();
console.log(`${ship1.name} changing top speed...`);
ship1.changeTopSpeed(8);
console.log(`${ship1.name} accelerating...`);
ship1.accelerate();

const ship2 = new SpaceShip('Millenium Falcon', 4);
console.log(`${ship2.name} accelerating...`);
ship2.accelerate();
console.log(`${ship2.name} changing top speed...`);
ship2.changeTopSpeed(7);
console.log(`${ship2.name} accelerating...`);
ship2.accelerate();
